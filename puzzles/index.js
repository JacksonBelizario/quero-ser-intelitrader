
const DotProduct = require('./dot-product');
const RoletaRomana = require('./roleta-romana');
const CampoMinado = require('./campo-minado');

console.log('=== Produto Escalar ===');

const dotProduct = new DotProduct();

let data = dotProduct.initArray(),
    data2 = dotProduct.initArray();

const produtoEscalar = dotProduct.result(data, data2);

console.log({data});
console.log({data2});
console.log({produtoEscalar});

console.log('\n=== Roleta Romana ===');

const qtdePessoas = 5;
const iniciarEm = 2;

console.log({qtdePessoas, iniciarEm});

const roletaRomana = new RoletaRomana(qtdePessoas, iniciarEm);

const sobrevivente = roletaRomana.sobrevivente();

console.log({sobrevivente});

console.log('\n=== Campo Minado ===');

const m = 4, n = 4;

const campoMinado = new CampoMinado(m, n);

const minas = campoMinado.minas();
const campo = campoMinado.campo();

console.log({minas});
console.log({campo});