
const Utils = require('../utils');

/**
 * http://dojopuzzles.com/problemas/exibe/roleta-romana/
 * Para se preparar para uma situação como essa, escreva um programa que
 * determine qual posição você deve ficar no círculo para poder sobreviver.
 * Você deverá informar a quantidade de pessoas no círculo (n > 0), a posição
 * da pessoa que irá começar o suicídio (1 <= i <= n) e o "passo", isto é, de
 * quantas em quantas pessoas, deverá ser morta (k > 0)
 *
 *     n > 0 pessoas são organizada em um círculo, numeradas de 1 a n em sentido horário;
 *     Iniciando na pessoa i, conta-se no sentido horário, até que se chegue na pessoa de valor k (k > 0), que é rapidamente morta.
 *     Continuamos a contar k pessoas no sentido horário, a partir da pessoa a esquerda da que foi morta.
 *     Esse processo é repetido indefinidamente, até que apenas uma pessoa seja a sobrevivente.
 *
 * Por exemplo quando n = 5, k = 2 e i = 1, a ordem de execuções é 2, 5, 3, e 1. O sobrevivente é 4.
 */
class RoletaRomana {

    /**
     * @param Integer length quantidade de pessoas
     * @param Integer startAt inicio suicidio
     */
    constructor(length, startAt) {
        this.pessoas = this._initArray(length);
        this.k = startAt;
    }

    _initArray(length) {
        return Utils.sequentialPopulate([], length);
    }

    sobrevivente() {
        const pessoas = this.pessoas;
        const n = pessoas.length;
        let k = this.k;

        while (pessoas.length > 1) {
            // posição do array do próximo a ser morto
            let kill;
            do {
                if (k > n) {
                    k -= n
                }
                
                kill = pessoas.indexOf(k);

                // Caso a próxima pessoa da roleta já tenha sido morta, escolhe o próximo vivo
                if (kill == -1) {
                    k += 1;
                }
            } while (kill == -1);

            pessoas.splice(kill, 1);

            // Próximo a ser morto
            k += 3;
        }
    
        // retorna o sobrevivente
        return pessoas[0];
    }
}

module.exports = RoletaRomana;