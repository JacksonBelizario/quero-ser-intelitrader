
const Utils = require('../utils');

/**
 * http://dojopuzzles.com/problemas/exibe/produto-escalar-de-vetores/
 * Definimos dois vetores A e B de dimensão n em termos de componentes como:
 * A = (a1, a2, a3, ..., an) e B = (b1, b2, b3, ..., bn)
 * O produto escalar entre esses vetores é descrito como:
 * A . B = a1 * b1 + a2 * b2 + a3 * b3 + ... + an * bn
 * Desenvolva um programa que, dado dois vetores de dimensão n, retorne o produto escalar entre eles.
 */
class DotProduct {

    constructor() {
        //
    }

    /**
     * Size of array to random populate
     */
    initArray(length = 5) {
        return Utils.randomPopulate([], length);
    }

    /**
     * Retorna o produto escalar entre dois vetores.
     * @param array data
     * @param array data2
     */
    result(data, data2) {
        if (!Array.isArray(data) || !Array.isArray(data2)) {
            throw new Error("Variável inválida");
        }
        if (data.length !== data2.length) {
            throw new Error("Os vetores possuem tamanhos diferentes");
        }

        return [...data]
            .map((o, i) => o * data2[i])
            .reduce((accum, curr) => accum + curr, 0);
    }
}

module.exports = DotProduct;