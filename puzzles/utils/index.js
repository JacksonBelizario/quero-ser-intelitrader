
class Utils {

    /**
     * Retorna um número aleatório dentre dois valores definidos
     */
    static getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    
    /**
     * Popula um vetor de tamanho definido pelo usuário
     * com números aleatórios entre 1 e 100
     */
    static randomPopulate = (data, length) => {
        for (let i = 0; i < length; i++) {
            data.push(Utils.getRandomInt(1, 100));
        }
        return data;
    }

    /**
     * Popula um vetor de tamnho definido pelo usuário
     * sequencialmente
     */
    static sequentialPopulate = (data, length) => {
        for (let i = 1; i <= length; i++) {
            data.push(i);
        }
        return data;
    }

    /**
     * A partir de determinada string de caracteres
     * retorna um caracter aleatório
     * @param String characters 
     */
    static getRandomChar(characters) {
        return characters.charAt(Math.floor(Math.random() * characters.length));
     }
}

module.exports = Utils;