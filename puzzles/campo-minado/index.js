
const Utils = require('../utils');

/**
 * http://dojopuzzles.com/problemas/exibe/campo-minado/
 * O objetivo deste jogo é encontrar onde estão todas as minas em um campo de tamanho MxN (M e N inteiros).
 * Para te ajudar, para cada posição, o jogo exibe o número de minas que são adjacentes a ela.
 *
 * Por exemplo, suponha o seguinte campo 4x4 com 2 minas (representadas pelo caractere *):
 * 
 *     * . . .
 *     . . . .
 *     . * . .
 *     . . . .
 *
 * Representando o mesmo campo, colocando os números de dicas como descrito acima, teremos:
 *
 *     * 1 0 0
 *     2 2 1 0
 *     1 * 1 0
 *     1 1 1 0
 * 
 * Como você pode perceber, cada quadrado pode ter até 8 quadrados adjacentes.
 * 
 * Sua tarefa é, dado a definição de um campo (definido por suas dimensões e pelo posicionamento das minas),
 * retornar o mesmo campo com as indicações de números de minas adjacentes em cada posição que não contenha uma mina.
 */
class CampoMinado {

    constructor(m, n) {
        this._m = m;
        this._n = n;
        this._gerarMinas();
        this._gerarDicas();
    }

    /**
     * Método responsável por criar a matriz de tamanho m*n
     * e gerar minas em posições aleatórias
     */
    _gerarMinas() {
        let m = this._m, n = this._n;
        
        let data = [], chars = '.*..';

        for (let i = 0; i < m; i++) {
            const data2 = [];
            for (let j = 0; j < n; j++) {
                data2.push(Utils.getRandomChar(chars));
            }
            data.push(data2);
        }

        this._minas = data;
    }

    /**
     * Método responsável por gerar as dicas de minas próximas
     * de uma determinada posição do campo minado
     */
    _gerarDicaPos(m, n) {
        const minas = this._minas;
        // Se a atual posição for uma mina não calcular a dica
        if (minas[m][n] === '*') {
            return '*';
        }
        let res = 0;
        for (let i = m-1; i <= m+1; i++) {
            for (let j = n-1; j <= n+1; j++) {
                // Caso i,j extrapole a mina não faz nada
                if (i < 0 || j < 0) {
                    continue;
                }
                // Caso i,j extrapole a mina não faz nada
                if (i > minas.length -1 || j > minas[i].length -1) {
                    continue;
                }
                // Posição calculada detectou uma mina, incrementa a dica
                if (minas[i][j] === '*') {
                    res += 1;
                }
            }
        }
        return res.toString();
    }

    /**
     * Método responsável por percorrer a matriz do campo minado e calcular as dicas
     */
    _gerarDicas() {
        let m = this._m, n = this._n;
        let data = [];
        for (let i = 0; i < m; i++) {
            const data2 = [];
            for (let j = 0; j < n; j++) {
                data2.push(this._gerarDicaPos(i, j));
            }
            data.push(data2);
        }
        this._campo = data;
    }

    minas() {
        return this._minas;
    }

    campo() {
        return this._campo;
    }
}

module.exports = CampoMinado;