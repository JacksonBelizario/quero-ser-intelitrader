# Puzzles
[Produto Escalar de Vetores](http://dojopuzzles.com/problemas/exibe/produto-escalar-de-vetores/)

[Roleta Romana](http://dojopuzzles.com/problemas/exibe/roleta-romana/)

[Campo Minado](http://dojopuzzles.com/problemas/exibe/campo-minado/)

### Linguagem utilizada
Utilizei a linguagem de programação Node.js para resolução dos puzzles escolhidos

### Como rodar

```
node puzzles/index.js
```