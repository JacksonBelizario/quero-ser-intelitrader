import React, {useEffect} from 'react';
import {
  SafeAreaView,
  BackHandler,
  View
} from 'react-native';

import {
  Button,
  Divider, TopNavigation, TopNavigationAction,
  Icon,
  Layout,
  Text,
  Input,
  Select,
} from '@ui-kitten/components';
import { StyleSheet } from 'react-native';

const BackIcon = (style) => (
  <Icon {...style} name='arrow-back' />
);

// Operações matemáticas disponíveis para o select
const dataSelect = [
  { text: '+' },
  { text: '-' },
  { text: '*' },
  { text: '/' },
];

export const HomeScreen = () => {
  
  useEffect(() => firstInput.current.focus(), []);

  let firstInput = React.createRef();

  const [fieldOne, setFieldOne] = React.useState('');
  const [fieldTwo, setFieldTwo] = React.useState('');
  const [result, setResult] = React.useState('');
  const [error, setError] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState(null);

  const navigateBack = () => {
    BackHandler.exitApp();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
  );

  const formatFloat = val => {
    return val.replace(/[^0-9\.]/g,'');
  }

  const hasDots = val => {
    const dots = val.match(/\./g);
    return dots && dots.length > 1;
  }

  // Altera o primeiro input
  const onChangeFieldOne = val => {
    // verifica se já possui ponto no campo
    if (hasDots(val)) {
      return;
    }
    // val = formatFloat(val);
    setFieldOne(formatFloat(val));
    // Limpa o campo que exibe o resultado anterior
    if (!!val) {
      setResult('');
    }
  }

  // Altera o primeiro input
  const onChangeFieldTwo = val => {
    // verifica se já possui ponto no campo
    if (hasDots(val)) {
      return;
    }
    // val = formatFloat(val);
    setFieldTwo(formatFloat(val));
    // Limpa o campo que exibe o resultado anterior
    if (!!val) {
      setResult('');
    }
  }

  // Calcula a operação matemática selecionada
  const calcular = () => {
    const value1 = parseFloat(fieldOne);
    const value2 = parseFloat(fieldTwo);
    if (isNaN(value1) || isNaN(value2) || !selectedOption) {
      return;
    }

    const {text} = selectedOption;

    // Em operações de divisão por zero, exibe um erro
    if (text == '/' && (value1 == 0 || value2 == 0)) {
      setError(true);
      setTimeout(() => {
        setError(false);
      }, 3000);
      return;
    }

    if (text == '+') {
      setResult(value1 + value2);
    }
    if (text == '-') {
      setResult(value1 - value2);
    }
    if (text == '*') {
      setResult(value1 * value2);
    }
    if (text == '/') {
      setResult(value1 / value2);
    }

    // Limpa os campos
    setFieldOne('');
    setFieldTwo('');
    setSelectedOption(null);

    // Foca o primeiro input após calcular
    firstInput.current.focus();
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='InteliCalculadora' alignment='center' leftControl={BackAction()}/>
      <Divider/>
      <Layout style={styles.container}>
        <View style={styles.inputsContainer}>
          <Input
            ref={firstInput}
            style={{flex:1, marginTop: 2}}
            placeholder=''
            value={fieldOne}
            onChangeText={onChangeFieldOne}
            keyboardType={'numeric'}
          />
          <Select
            style={{flex:1, marginHorizontal: 10}}
            size='large'
            data={dataSelect}
            selectedOption={selectedOption}
            onSelect={setSelectedOption}
          />
          <Input
            style={{flex:1, marginTop: 2}}
            placeholder=''
            value={fieldTwo}
            onChangeText={onChangeFieldTwo}
            keyboardType={'numeric'}
          />
        </View>
        <Button
          style={styles.resButton}
          size='giant'
          disabled={fieldOne == '' || fieldTwo == '' || !selectedOption}
          onPress={calcular}> = </Button>
        { error && <Text style={styles.alert} status='danger'>Operação inválida</Text> }
        <Text style={styles.result} category='h1'>
          {result}
        </Text>
      </Layout>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10
  },
  inputsContainer: {
    marginTop: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  text: {
    textAlign: 'center',
  },
  resButton: {
    marginVertical: 30,
    fontSize: 26
  },
  alert: {
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderColor: '#FF463D',
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20
  },
  result: {
    textAlign: 'center',
    width: '100%',
    borderColor: '#c1c1c1',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 8
  }
});
